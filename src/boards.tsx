import { Board } from "./types";

const boards: Board[] = [
  {
    name: "SWOT",
    description:
      "SWOT analysis (or SWOT matrix) is a strategic planning technique used to help a person or organization identify strengths, weaknesses, opportunities, and threats related to business competition or project planning.",
    image: "/images/swot.png",
    layout: {
      orientation: "row",
      items: [
        {
          orientation: "column",
          items: [
            { name: "Strengths", color: "#9ee37d" },
            { name: "Opportunities", color: "#ffe66d" },
          ],
        },
        {
          orientation: "column",
          items: [
            { name: "Weaknesses", color: "#88bedb" },
            { name: "Threats", color: "#f78791" },
          ],
        },
      ],
    },
    about: [
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "SWOT analysis (or SWOT matrix) is a strategic planning technique used to help a person or organization identify strengths, weaknesses, opportunities, and threats related to business competition or project planning. This technique, which operates by ‘peeling back layers of the company’s designed for use in the preliminary stages of decision-making processes and can be used as a tool for evaluation of the strategic position of organizations of many kinds (for-profit enterprises, local and national governments, NGOs, etc.). It is intended to specify the objectives of the business venture or project and identify the internal and external factors that are favorable and unfavorable to achieving those objectives. Users of a SWOT analysis often ask and answer questions to generate meaningful information for each category to make the tool useful and identify their competitive advantage. SWOT has been described as the tried-and-true tool of strategic analysis but has also been criticized for its limitations.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "SWOT assumes that strengths and weaknesses are frequently internal, while opportunities and threats are more commonly external. The name is an acronym for the four parameters the technique examines:",
          },
        ],
      },
      {
        type: "list",
        items: [
          {
            type: "text",
            text: "Strengths: characteristics of the business or project that give it an advantage over others.",
          },
          {
            type: "text",
            text: "Weaknesses: characteristics that place the business or project at a disadvantage relative to others.",
          },
          {
            type: "text",
            text: "Opportunities: elements in the environment that the business or project could exploit to its advantage.",
          },
          {
            type: "text",
            text: "Threats: elements in the environment that could cause trouble for the business or project.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "The degree to which the internal environment of the firm matches with the external environment is expressed by the concept of strategic fit. Identification of SWOTs is important because they can inform later steps in planning to achieve the objective. First, decision-makers should consider whether the objective is attainable, given the SWOTs. If the objective is not attainable, they must select a different objective and repeat the process.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Some authors attribute SWOT analysis to Albert Humphrey, who led a convention at the Stanford Research Institute (now SRI International) in the 1960s and 1970s using data from Fortune 500 companies. However, Humphrey himself did not claim the creation of SWOT, and the origins remain obscure.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "SWOT analysis can be used in any decision-making situation when a desired end-state (objective) is defined, not just profit-seeking organizations. Examples include non-profit organizations, governmental units, and individuals. SWOT analysis may also be used in pre-crisis planning and preventive crisis management. SWOT analysis may also be used in creating a recommendation during a viability study|survey.",
          },
        ],
      },
    ],
  },
  {
    name: "PEST",
    description:
      "PEST analysis (political, economic, socio-cultural, and technological) describes a framework of macro-environmental factors used in the environmental scanning component of strategic management.",
    image: "/images/pest.png",
    layout: {
      orientation: "row",
      items: [
        { name: "Political", color: "#932432" },
        { name: "Economical", color: "#3C1874" },
        { name: "Social", color: "#283747" },
        { name: "Technological", color: "#F3F3F3" },
      ],
    },
    about: [
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "PEST analysis (political, economic, socio-cultural, and technological) describes a framework of macro-environmental factors used in the environmental scanning component of strategic management. It is part of an external analysis when conducting a strategic analysis or doing market research, and gives an overview of the different macro-environmental factors to be taken into consideration. It is a strategic tool for understanding market growth or decline, business position, potential, and direction for operations.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "The basic PEST analysis includes four factors:",
          },
        ],
      },
      {
        type: "list",
        items: [
          {
            type: "text",
            text: "Political factors relate to how the government intervenes in the economy. Specifically, political factors have areas including tax policy, labor law, environmental law, trade restrictions, tariffs, and political stability. Political factors may also include goods and services which the government aims to provide or be provided (merit goods) and those that the government does not want to be provided (demerit goods or merit inferiors). Furthermore, governments have a high impact on the health, education, and infrastructure of a nation.",
          },
          {
            type: "text",
            text: "Economic factors include economic growth, exchange rates, inflation rate, and interest rates. These factors can drastically affect how a business operates. For example, interest rates affect a firm’s cost of capital and therefore to what extent a business grows and expands.",
          },
          {
            type: "text",
            text: "Social factors include the cultural aspects and health consciousness, population growth rate, age distribution, career attitudes, and emphasis on safety. High trends in social factors affect the demand for a company's products and how that company operates. For example, the aging population may imply a smaller and less willing workforce (thus increasing the cost of labor). Furthermore, companies may change various management strategies to adapt to social trends caused by this (such as recruiting older workers).",
          },
          {
            type: "text",
            text: "Technological factors include technological aspects like R&D activity, automation, technology incentives, and the rate of technological change. These can determine barriers to entry, minimum efficient production level and influence the outsourcing decisions. Furthermore, technological shifts would affect costs, quality, and lead to innovation.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Variants that build on the PEST framework include:",
          },
        ],
      },
      {
        type: "list",
        items: [
          {
            type: "text",
            text: "PESTEL or PESTLE, which adds legal and environmental factors.",
          },
          {
            type: "text",
            text: "SLEPT, adding legal factors. STEPE, adding ecological factors.",
          },
          {
            type: "text",
            text: "STEEPLE and STEEPLED, adding ethics and demographic factors (occasionally rendered as PESTLEE).",
          },
          {
            type: "text",
            text: "DESTEP, adding demographic and ecological factors.",
          },
          {
            type: "text",
            text: "SPELIT, adding legal and intercultural factors, popular in the United States since the mid-2000s.",
          },
          {
            type: "text",
            text: "PMESII-PT, a form of environmental analysis that looks at the aspects of political, military, economic, social, information, infrastructure, the physical environment, and time aspects in a military context.",
          },
        ],
      },
    ],
  },
  {
    name: "PESTLE",
    description:
      "The PESTLE analysis is a tool used as situational analysis for business evaluation purposes.",
    image: "/images/pestel.png",
    layout: {
      orientation: "row",
      items: [
        {
          name: "Political",
          color: "#A8E6CE",
        },
        {
          name: "Economical",
          color: "#DCEDC2",
        },
        {
          name: "Social",
          color: "#FFD3B5",
        },
        {
          name: "Technological",
          color: "#FFAAA6",
        },
        {
          name: "Legal",
          color: "#FF8C94",
        },
        {
          name: "Ecological",
          color: "#5B84B1",
        },
      ],
    },
    about: [
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "The PESTLE analysis is a tool used as situational analysis for business evaluation purposes. This tool is one of the most frequently applied models in the evaluation of the highly dynamic external business environment. Aside from business, it is now being employed as a method in research due to its usefulness. A growing number of studies applied this analytical tool in different sustainable projects, including the evaluation of external factors affecting management decisions for coastal zone and freshwater resources, development of sustainable buildings, sustainable energy solutions, and transportation.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Expanding the PEST analysis, PESTLE adds:",
          },
        ],
      },
      {
        type: "list",
        items: [
          {
            type: "text",
            text: "Legal factors include discrimination law, consumer law, antitrust law, employment law, and health and safety law. These factors can affect how a company operates, its costs, and the demand for its products.",
          },
          {
            type: "text",
            text: "Environmental factors include ecological and environmental aspects such as weather, climate, and climate change, which may especially affect industries such as tourism, farming, and insurance. Furthermore, growing awareness of the potential impacts of climate change is affecting how companies operate and the products they offer, both creating new markets and diminishing or destroying existing ones.",
          },
        ],
      },
    ],
  },
  {
    name: "Five Forces",
    description:
      "Porter’s Five Forces Framework is a method for analyzing the competition of a business.",
    image: "/images/porters-five-forces.png",
    layout: {
      orientation: "row",
      items: [
        {
          orientation: "column",
          items: [
            { name: "Barriers to entry", color: "#E86850" },
            { name: "Supplier's bargaining power", color: "#4D774E" },
          ],
        },
        {
          orientation: "column",
          items: [{ name: "Industry Rivalry", color: "#9DC88D" }],
        },
        {
          orientation: "column",
          items: [
            { name: "Threats of substitutes", color: "#F1B24A" },
            { name: "Bargaining power of buyers", color: "#39603D" },
          ],
        },
      ],
    },
    about: [
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Porter’s Five Forces Framework is a method for analyzing the competition of a business. It draws from industrial organization (IO) economics to derive five forces that determine the competitive intensity and, therefore, the attractiveness (or lack thereof) of an industry in terms of its profitability. An “unattractive” industry is one in which the effect of these five forces reduces overall profitability. The most unattractive industry would be one approaching “pure competition”, in which available profits for all firms are driven to normal profit levels. The five-forces perspective is associated with its originator, Michael E. Porter of Harvard University. This framework was first published in Harvard Business Review in 1979.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Porter refers to these forces as the microenvironment, to contrast it with the more general term macro environment. They consist of those forces close to a company that affects its ability to serve its customers and make a profit. A change in any of the forces normally requires a business unit to re-assess the marketplace given the overall change in industry information. The overall industry attractiveness does not imply that every firm in the industry will return the same profitability. Firms are able to apply their core competencies, business model, or network to achieve a profit above the industry average. A clear example of this is the airline industry. As an industry, profitability is low because the industry's underlying structure of high fixed costs and low variable costs afford enormous latitude in the price of airline travel. Airlines tend to compete on cost, and that drives down the profitability of individual carriers as well as the industry itself because it simplifies the decision by a customer to buy or not buy a ticket. A few carriers - Richard Branson’s Virgin Atlantic is one - have tried, with limited success, to use sources of differentiation in order to increase profitability.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Porter's five forces include three forces from ”'horizontal” competition - the threat of substitute products or services, the threat of established rivals, and the threat of new entrants - and two others from “vertical”' competition - the bargaining power of suppliers and the bargaining power of customers.",
          },
        ],
      },
      {
        type: "paragraph",
        items: [
          {
            type: "text",
            text: "Porter developed his five forces framework in reaction to the then-popular SWOT analysis, which he found both lacking in rigor and ad hoc. The Five-forces framework is based on the structure–conduct–performance paradigm in industrial organizational economics. Other Porter strategy tools include the value chain and generic competitive strategies.",
          },
        ],
      },
    ],
  },
];

export default boards;
