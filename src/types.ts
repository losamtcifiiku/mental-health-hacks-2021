export function hasOwnProperty<X extends {}, Y extends PropertyKey>(
  obj: X,
  prop: Y
): obj is X & Record<Y, unknown> {
  return obj.hasOwnProperty(prop);
}

export function isStickerBoard(val: unknown): val is StickerBoard {
  return (
    typeof val === "object" &&
    val !== null &&
    hasOwnProperty(val, "name") &&
    hasOwnProperty(val, "color") &&
    typeof val.name === "string" &&
    typeof val.color === "string"
  );
}

export function isLayout(val: unknown): val is Layout {
  return (
    typeof val === "object" &&
    val !== null &&
    hasOwnProperty(val, "orientation") &&
    hasOwnProperty(val, "items") &&
    (val.orientation === "vertical" || val.orientation === "horizontal") &&
    (isStickerBoard(val.items) || isLayout(val.items))
  );
}

export function parseJSON(text: string): unknown {
  return JSON.parse(text);
}

export type HeaderNavProps = {
  links: Array<{ path: string; name: string }>;
};

export type CardT = {
  logo: string;
  text: string;
  link: string;
};

export type Point = {
  x: number;
  y: number;
};

export type StickerViewT = {
  state: "view";
  title: string;
  text: string;
  pos: Point;
};

export type StickerEditT = {
  state: "edit";
  title: string;
  text: string;
  pos: Point;
};

export type StickerT = StickerViewT | StickerEditT;

export const emptySticker: StickerT = {
  state: "edit",
  pos: { x: 0, y: 0 },
  title: "",
  text: "",
};

export type MarkupText = {
  type: "text";
  text: string;
};

export type MarkupParagraph = {
  type: "paragraph";
  items: Markup[];
};

export type MarkupList = {
  type: "list";
  items: Markup[];
};

export type MarkupLink = {
  type: "link";
  link: string;
  child: Markup[];
};

export type Markup = MarkupText | MarkupParagraph | MarkupList | MarkupLink;

export type Orientation = "column" | "row";

export type StickerBoard = {
  name: string;
  color: string;
};

export type Layout = {
  orientation: Orientation;
  items: Array<StickerBoard | Layout>;
};

export type Board = {
  name: string;
  description: string;
  image: string;
  layout: Layout;
  about: Markup[];
};

export type StickerProps = {
  stickers: any;
  onStickerAdd: (index: number[], sticker: StickerT) => void;
  onStickerChange: (index: number[], sticker: StickerT) => void;
  onStickerRemove: (indeex: number[]) => void;
};
