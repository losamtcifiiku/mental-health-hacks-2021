import { h, Component } from "preact";
import { emptySticker, StickerProps, StickerT } from "../types";
import { StickerBoard as StickerBoardT } from "../types";
import Button from "./Button";
import Sticker from "./Sticker";
import "../css/StickerBoard.scss";
import Icon from "./Icon";

type Props = StickerProps & {
  board: StickerBoardT;
};

export default class StickerBoard extends Component<Props> {
  onDrop(evt: DragEvent) {
    evt.preventDefault();

    const stickerStr = evt.dataTransfer?.getData("application/json");

    if (stickerStr === undefined) {
      return;
    }

    const sticker = JSON.parse(stickerStr) as StickerT;

    this.props.onStickerAdd([], sticker);
  }

  render() {
    return (
      <div
        className="sticker-board"
        style={{
          backgroundColor: this.props.board.color,
        }}
        onDragOver={e => e.preventDefault()}
        onDrop={this.onDrop.bind(this)}>
        <div class="header">
          <p className="title">{this.props.board.name}</p>
          <Button onClick={() => this.props.onStickerAdd([], emptySticker)}>
            <Icon icon="plus" />
          </Button>
        </div>
        <div style={{ display: "flex", gap: "1em", flexWrap: "wrap" }}>
          {this.props.stickers.map((sticker: StickerT, i: number) => (
            <Sticker
              {...sticker}
              onChange={this.props.onStickerChange.bind(this, [i])}
              onLeave={this.props.onStickerRemove.bind(this, [i])}
            />
          ))}
        </div>
      </div>
    );
  }
}
