import { h, FunctionComponent } from "preact";

type Props = {
  icon: string | string[];
};

const Icon: FunctionComponent<Props> = ({ icon }) => {
  let icons: string[];

  if (typeof icon === "string") {
    icons = [icon];
  } else {
    icons = icon;
  }

  return <i class={icons.reduce((s, c) => `${s} fa-${c}`, "fas")}></i>;
};

export default Icon;
