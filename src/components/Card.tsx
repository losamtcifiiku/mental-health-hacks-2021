import { h, Fragment, FunctionComponent } from "preact";
import { route } from "preact-router";
import { CardT } from "../types";
import "../css/Card.scss";

const Card: FunctionComponent<CardT> = ({ link, logo, text }) => (
  <>
    <div className="card" onClick={() => route(link)}>
      <img src={logo} alt={text} style={{ width: "100%" }} />
      <p>{text}</p>
    </div>
  </>
);

export default Card;
