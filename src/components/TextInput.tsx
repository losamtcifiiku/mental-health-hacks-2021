import { FunctionComponent, h } from "preact";
import "../css/TextInput.scss";

type Props = {
  text: string;
  placeholder?: string;
  onChange(text: string): void;
  onEnter(): void;
};

const TextInput: FunctionComponent<Props> = ({
  text,
  placeholder,
  onChange,
  onEnter,
}) => (
  <input
    className="text-input"
    type="text"
    value={text}
    placeholder={placeholder}
    onInput={e => onChange((e.target as HTMLInputElement).value)}
    onKeyDown={e => {
      if (e.code === "Enter") {
        onEnter();
      }
    }}
  />
);

export default TextInput;
