import { h, FunctionComponent, createRef } from "preact";
import Button from "./Button";

type Props = {
  onFile(file: File): void;
};

const FileInput: FunctionComponent<Props> = ({ onFile, children }) => {
  const ref = createRef<HTMLInputElement>();

  return (
    <label>
      <input
        ref={ref}
        type="file"
        hidden
        onChange={e =>
          Array.from((e.target as HTMLInputElement).files || []).map(onFile)
        }
      />
      <Button onClick={() => ref.current?.click()}>{children}</Button>
    </label>
  );
};

export default FileInput;
