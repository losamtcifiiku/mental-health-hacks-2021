import { h, FunctionComponent } from "preact";
import { HeaderNavProps } from "../types";
import { Link } from "preact-router";
import "../css/Header.scss";

const App: FunctionComponent<HeaderNavProps> = ({ links }) => (
  <header>
    <a className='logo' href="/">Éminence grise</a>
    <nav>
      {links.map(({ path, name }) => (
        <Link href={path} className="header-top__navbar-item">
          {name}
        </Link>
      ))}
    </nav>
  </header>
);

export default App;
