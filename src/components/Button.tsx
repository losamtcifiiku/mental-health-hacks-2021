import { FunctionComponent, h } from "preact";
import "../css/Button.scss";

type Props = {
  onClick?(): void;
};

const Button: FunctionComponent<Props> = ({ onClick, children }) => (
  <button className="button" onClick={onClick}>
    {children}
  </button>
);

export default Button;
