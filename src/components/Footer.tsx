import { h, Fragment } from "preact";
import Icon from "./Icon";
import "../css/Footer.scss";

export default () => (
  <>
    <footer>
      <div className="footer-license" style={{ whiteSpace: "pre-wrap" }}>
        Licensed under <a href="https://www.gnu.org/licenses">GPL</a>
      </div>
      <div className="icons">
        <a href="https://gitlab.com/losamtcifiiku/mental-health-hacks-2021">
          <Icon icon={["code", "2x"]} />
        </a>
        <a href="https://devpost.com/software/eminence-grise">
          <Icon icon={["bookmark", "2x"]} />
        </a>
      </div>
      <div className="footer-copyright">
        © 2021 losamtcifiiku team. All rights reversed.
      </div>
    </footer>
  </>
);
