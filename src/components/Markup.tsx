import { h, Fragment } from "preact";
import { Link } from "preact-router";
import { Markup as MarkupT } from "../types";

export default function Markup({ markup }: { markup: MarkupT }) {
  switch (markup.type) {
    case "text":
      return <>{markup.text}</>;
    case "link":
      return (
        <Link href={markup.link}>
          {markup.child.map(item => (
            <Markup markup={item} />
          ))}
        </Link>
      );
    case "paragraph":
      return (
        <p>
          {markup.items.map(markup => (
            <Markup markup={markup} />
          ))}
        </p>
      );
    case "list":
      return (
        <ul>
          {markup.items.map(innerItem => (
            <li>
              <Markup markup={innerItem} />
            </li>
          ))}
        </ul>
      );
  }
}
