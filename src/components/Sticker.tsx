import { h, Component, createRef } from "preact";
import { StickerT } from "../types";
import Button from "./Button";
import "../css/Sticker.scss";
import TextInput from "./TextInput";
import "../css/Sticker.scss";
import Icon from "./Icon";

type State = StickerT;

type Props = State & {
  onChange: (state: State) => void;
  onLeave: () => void;
};

type StateT = { drag: boolean };

export default class Sticker extends Component<Props, StateT> {
  elRoot = createRef<HTMLDivElement>();

  state: StateT = {
    drag: false,
  };

  setTitle(title: string) {
    if (this.props.state === "edit") {
      const { text, pos } = this.props;
      this.props.onChange({ state: "edit", title, text, pos });
    }
  }

  setText(text: string) {
    if (this.props.state === "edit") {
      const { title, pos } = this.props;
      this.props.onChange({ state: "edit", title, text, pos });
    }
  }

  onView() {
    const { title, text, pos } = this.props;
    this.props.onChange({ state: "view", title, text, pos });
  }

  onEdit() {
    const { title, text, pos } = this.props;
    this.props.onChange({ state: "edit", title, text, pos });
  }

  onDragStart(evt: DragEvent) {
    if (evt.dataTransfer !== null) {
      evt.dataTransfer.setData("application/json", JSON.stringify(this.props));
      evt.dataTransfer.effectAllowed = "move";
      evt.dataTransfer.dropEffect = "move";
    }

    this.setState({ drag: true });
  }

  onDragEnd() {
    this.setState({ drag: false });
    this.onView();
    this.props.onLeave && this.props.onLeave();
  }

  onMouseUp() {
    this.onView();
  }

  render() {
    return (
      <div
        ref={this.elRoot}
        class={this.state.drag ? "sticker dragging" : "sticker"}
        style={{
          cursor:
            this.props.state === "view" || this.state.drag ? "move" : "auto",
        }}
        draggable={this.props.state === "view"}
        onDragStart={this.onDragStart.bind(this)}
        onDragEnd={this.onDragEnd.bind(this)}>
        <div className="button-box">
          <Button
            onClick={
              this.props.state === "edit"
                ? this.onView.bind(this)
                : this.onEdit.bind(this)
            }>
            <Icon
              icon={this.props.state === "edit" ? "check-circle" : "edit"}
            />
          </Button>
          <Button onClick={this.props.onLeave}>
            <Icon icon="trash" />
          </Button>
        </div>
        {this.props.state === "edit" ? (
          <TextInput
            text={this.props.title}
            placeholder="Title"
            onChange={this.setTitle.bind(this)}
            onEnter={this.onView.bind(this)}
          />
        ) : (
          <p className="title">{this.props.title}</p>
        )}
        {this.props.state === "edit" ? (
          <TextInput
            text={this.props.text}
            placeholder="Text"
            onChange={this.setText.bind(this)}
            onEnter={this.onView.bind(this)}
          />
        ) : (
          <p>{this.props.text}</p>
        )}
      </div>
    );
  }
}
