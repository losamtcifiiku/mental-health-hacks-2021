import { Component, h } from "preact";
import StickerBoard from "./StickerBoard";
import { Layout, StickerProps, StickerT } from "../types";
import "../css/BoardLayout.scss";

type Props = StickerProps & {
  layout: Layout;
};

export default class BoardLayout extends Component<Props> {
  onStickerAdd(i: number, ix: number[], sticker: StickerT) {
    this.props.onStickerAdd([i, ...ix], sticker);
  }

  onStickerChange(i: number, ix: number[], s: any) {
    this.props.onStickerChange([i, ...ix], s);
  }

  onStickerRemove(i: number, ix: number[]) {
    this.props.onStickerRemove([i, ...ix]);
  }

  render() {
    return (
      <div
        className="board-layout"
        style={{ flexDirection: this.props.layout.orientation }}>
        {this.props.layout.items.map((item, index) =>
          typeof item === "object" && "name" in item ? (
            <StickerBoard
              board={item}
              stickers={this.props.stickers[index] || []}
              onStickerAdd={this.onStickerAdd.bind(this, index)}
              onStickerChange={this.onStickerChange.bind(this, index)}
              onStickerRemove={this.onStickerRemove.bind(this, index)}
            />
          ) : (
            <BoardLayout
              layout={item}
              stickers={this.props.stickers[index] || []}
              onStickerAdd={this.onStickerAdd.bind(this, index)}
              onStickerChange={this.onStickerChange.bind(this, index)}
              onStickerRemove={this.onStickerRemove.bind(this, index)}
            />
          )
        )}
      </div>
    );
  }
}
