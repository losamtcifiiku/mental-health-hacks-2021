import { h, Fragment, Component } from "preact";
import { route } from "preact-router";
import BoardLayout from "../components/BoardLayout";
import Button from "../components/Button";
import FileInput from "../components/FileInput";
import { Board as BoardT, hasOwnProperty, parseJSON, StickerT } from "../types";

type Props = { board: BoardT };
type State = { stickers: any };

export default class Board extends Component<Props, State> {
  state: State = {
    stickers: [],
  };

  onStickerAdd(index: number[], sticker: StickerT) {
    const reduce = (list: unknown, index: number[], value: StickerT): any => {
      if (index.length === 0) {
        if (list instanceof Array) {
          return [...list, value];
        } else {
          return [value];
        }
      }

      if (!(list instanceof Array)) {
        return [reduce(undefined, index.slice(1), value)];
      }

      const newList = list.slice();

      newList[index[0]] = reduce(newList[index[0]], index.slice(1), value);
      return newList;
    };

    const newState = {
      stickers: reduce(this.state.stickers, index, sticker),
    };
    this.setState(newState);
  }

  onStickerChange(index: number[], sticker: StickerT) {
    const reduce = (list: any[], index: number[], value: StickerT) => {
      if (index.length === 0) {
        return value;
      }

      const newList = list.slice();
      newList[index[0]] = reduce(newList[index[0]], index.slice(1), value);
      return newList;
    };

    this.setState({
      stickers: reduce(this.state.stickers, index, sticker),
    });
  }

  onStickerRemove(index: number[]) {
    const reduce = (list: any[], index: number[]) => {
      if (index.length === 1) {
        const newList = list.slice();
        newList.splice(index[0], 1);
        return newList;
      }

      const newList = list.slice();
      newList[index[0]] = reduce(newList[index[0]], index.slice(1));
      return newList;
    };

    this.setState({
      stickers: reduce(this.state.stickers, index),
    });
  }

  export() {
    const el = document.createElement("a");
    el.href = URL.createObjectURL(
      new Blob([JSON.stringify(this.state)], {
        type: "application/json",
      })
    );
    el.download = `${this.props.board.name}.json`;
    el.dispatchEvent(
      new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
      })
    );
    el.remove();
  }

  validateStickers(stickers: unknown): stickers is StickerT {
    if (
      typeof stickers === "object" &&
      stickers !== null &&
      hasOwnProperty(stickers, "state") &&
      hasOwnProperty(stickers, "title") &&
      hasOwnProperty(stickers, "text") &&
      (stickers.state === "edit" ||
        stickers.state === "drag" ||
        stickers.state === "view") &&
      typeof stickers.title === "string" &&
      typeof stickers.text === "string"
    ) {
      return true;
    }

    if (stickers instanceof Array) {
      for (const sticker of stickers) {
        if (!this.validateStickers(sticker)) {
          return false;
        }
      }
    }

    return true;
  }

  async import(file: File) {
    const text = await file.text();
    const json = parseJSON(text);

    if (
      typeof json === "object" &&
      json !== null &&
      hasOwnProperty(json, "stickers") &&
      this.validateStickers(json.stickers)
    ) {
      this.setState(json);
    }
  }

  render() {
    const { name, layout } = this.props.board;

    return (
      <>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            gap: "1em",
            marginBottom: "1em",
          }}>
          <Button onClick={() => route(`/boards/${name}/about`)}>
            Learn more
          </Button>
          <Button onClick={this.export.bind(this)}>Export</Button>
          <FileInput onFile={this.import.bind(this)}>Import</FileInput>
        </div>
        <BoardLayout
          layout={layout}
          stickers={this.state.stickers}
          onStickerAdd={this.onStickerAdd.bind(this)}
          onStickerChange={this.onStickerChange.bind(this)}
          onStickerRemove={this.onStickerRemove.bind(this)}
        />
      </>
    );
  }
}
