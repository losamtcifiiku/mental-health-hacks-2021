import { h, Fragment } from "preact";

export default () => (
  <>
    <h2>About us</h2>
    <p>
      Brain hacking is the application of techniques and/or technologies to
      affect an individual’s mental state, cognitive processes, or level of
      function. Such efforts may be intentionally applied by the individual as a
      tool for personal development or by an employer as a part of employee
      wellness and productivity initiatives.
    </p>
    <p>
      As an approach for personal development, brain hacking seeks to enhance
      cognitive function and optimize efficacy and happiness. Brain-centered
      approaches to this end include meditation, metacognitive therapy,
      attention training, and memory training. Continuing to learn and challenge
      the brain is essential; things like learning a new language or studying a
      new discipline improve the brain’s ability to function. Recent research
      indicates that, for the greatest effect, learning experiences must be
      somewhat difficult and require effort. A more holistic approach to brain
      hacking includes all the recommendations for health and wellbeing, such as
      a nutritious and varied diet, adequate exercise, involvement in a
      community, and time spent outdoors in nature.
    </p>
    <p>
      Brain hacking is also used by external parties such as marketers and
      product designers to influence individual behavior. Social engineering,
      which exploits individual vulnerabilities to conduct security breaches,
      can also be considered a type of brain hacking. When the intention is to
      manipulate the target’s behavior without their knowledge or consent, brain
      hacking is sometimes referred to as brain hijacking.
    </p>
    <p>
      Marketers and product designers adopt principles from neuroscience,
      behavioral psychology, and sociology to develop compulsive elements for
      user interfaces, software Mobile apps, social media, games, and marketing
      content. It is very difficult to make a decision these days because the
      competition has made the choice so great that there is simply not enough
      time to fully analyze the situation. Therefore, mankind faces such
      problems every day.
    </p>
    <p>
      And we present our solution -{" "}
      <a href="https://en.wikipedia.org/wiki/%C3%89minence_grise">
        Éminence grise
      </a>
      . Have you often had a difficult decision to think about?
    </p>
    <p>
      Our solution will help you make the right decision by giving you a wide
      range of different decision boards, where you can fully analyze each
      decision from a variety of angles - just get the table right, and a short
      description of each of them will help you with that.
    </p>
  </>
);
