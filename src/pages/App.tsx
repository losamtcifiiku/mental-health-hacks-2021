import { h, Fragment, render } from "preact";
import AsyncRoute from "preact-async-route";
import Router from "preact-router";
import About from "./About";
import Header from "../components/Header";
import Home from "./Home";
import boards from "../boards";
import AboutBoard from "./AboutBoard";
import Board from "./Board";
import Footer from "../components/Footer";
import "@fortawesome/fontawesome-free/css/all.css";
import "../css/App.scss";

type Route = {
  path: string;
  name: string;
  component: any;
};

const routes: Array<Route> = [
  { path: "/", name: "Home", component: Home },
  { path: "/about", name: "About", component: About },
];

const boardRoutes: Array<Route> = boards.map(
  board =>
    ({
      path: `/boards/${board.name}`,
      name: board.name,
      component: () => <Board board={board} />,
    } as Route)
);

const aboutBoardRoutes: Array<Route> = boards.map(
  board =>
    ({
      path: `/boards/${board.name}/about`,
      name: board.name,
      component: () => <AboutBoard board={board} />,
    } as Route)
);

const headerRoutes: Array<Route> = [...routes, ...boardRoutes];

const allRoutes: Array<Route> = [
  ...routes,
  ...boardRoutes,
  ...aboutBoardRoutes,
];

const App = () => (
  <>
    <Header links={headerRoutes} />
    <main>
      <Router>
        {allRoutes.map(({ path, component }) => (
          <AsyncRoute path={path} component={component} />
        ))}
      </Router>
    </main>
    <Footer />
  </>
);

render(<App />, document.body);
