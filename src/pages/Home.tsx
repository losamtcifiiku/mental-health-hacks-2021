import { h, Fragment } from "preact";
import boards from "../boards";
import Card from "../components/Card";
import "../css/Home.scss";

export default () => (
  <>
    <div className="home">
      {boards.map(({ name, description, image }) => (
        <Card link={`/boards/${name}`} text={description} logo={image} />
      ))}
    </div>
  </>
);
