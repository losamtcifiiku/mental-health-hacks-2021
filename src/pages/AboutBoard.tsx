import { h, Fragment } from "preact";
import { route } from "preact-router";
import Button from "../components/Button";
import Markup from "../components/Markup";
import { Board } from "../types";
import "../css/AboutBoard.scss";

export default function AboutBoard(props: { board: Board }) {
  const { name, about } = props.board;
  return (
    <div className="about-board">
      <p className="title">About {name}</p>
      <Button onClick={() => route(`/boards/${name}`)}>Go back</Button>
      {about.map(item => (
        <Markup markup={item} />
      ))}
    </div>
  );
}
