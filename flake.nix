{
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ git git-lfs nodejs yarn ];
        };

        defaultPackage = null;
        defaultApp = flake-utils.lib.mkApp { drv = defaultPackage; };
      });
}
